import { LanguageContext } from './LanguageContext';

export class NotebookSegment {
	id: string;

	type: string;

	contextName: string;
	context: LanguageContext;
	code: string;
}

export class Notebook {
	name: string;
	segments: NotebookSegment[];

	path?: string;
	config?: any;

	constructor(_name: string, _segments: NotebookSegment[]){
		this.name = _name;
		this.segments = _segments;
	}
}