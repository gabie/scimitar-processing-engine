export class ContextMenuOption{
	text?: string;
	annotation?: string;
	
	action?: any;
	validation?: any;

	separator?: boolean;
}

export class ContextMenu {
	name: string;
	options: ContextMenuOption[];
}

export class TopMenuOption {
	text: string;
	menu: ContextMenu;
}