export class IpcModelResponse<T>{
    error: boolean;
    message: T;

    constructor(_message: T, _error: boolean = false){
        this.message = _message;
        this.error = _error;
    }
}