export class LanguageExecution {
	command: string;
	args: string[];
}

export class ErrorPattern {
	regex: string;
	lineCapture: number;
}

export class LanguageContext {
	extension: string;
	syntax: string;
	sampleCode: string;

	error: ErrorPattern;
	execution: LanguageExecution;	
}