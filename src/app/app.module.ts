import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CodeEngineComponent } from './components/code-engine/code-engine.component';
import { ProcessorBodyComponent } from './components/processor-body/processor-body.component';

import { HotkeyModule } from 'angular2-hotkeys';
import { KatexModule } from 'ng-katex';
import { AngularSplitModule } from 'angular-split';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NotebookComponent } from './components/notebook/notebook.component';
import { ContextMenuComponent } from './components/context-menu/context-menu.component';
import { TextEngineComponent } from './components/text-engine/text-engine.component';
import { ConsoleOutputComponent } from './components/console-output/console-output.component';

const routes: Routes = [
	{ path: 'processor', component: ProcessorBodyComponent },

	{
		path: '',
		redirectTo: '/processor',
		pathMatch: 'full'
	}
];

@NgModule({
	declarations: [
		AppComponent,
		CodeEngineComponent,
		ProcessorBodyComponent,
		SidebarComponent,
		NotebookComponent,
		ContextMenuComponent,
		TextEngineComponent,
		ConsoleOutputComponent,
	],
	imports: [
		RouterModule.forRoot(routes),
		BrowserModule,
		FormsModule,
		KatexModule,
		AngularSplitModule.forRoot(),
		HotkeyModule.forRoot()
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}