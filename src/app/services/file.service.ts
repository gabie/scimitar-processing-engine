import { Injectable } from '@angular/core';
import { IpcService } from './ipc.service';
import * as path from 'path';
import { Notebook } from '../model/Notebook';
import { ContextService } from './context.service';
import { IdGenerationService } from './id-generation.service';

@Injectable({
	providedIn: 'root'
})
export class FileService {

	tempFolder: string = "temp";
	codeFolder: string = "code";

	constructor(
		private ipc: IpcService,
		private ids: IdGenerationService
	) { }

	toAbsolutePath(file: string) {
		return this.ipc.nodeEvent<string>("toAbsolutePath", "toAbsolutePathResponse", file);
	}

	relativeFileExists(file: string) {
		return this.ipc.nodeEvent<boolean>("existsRelativeFile", "existsRelativeFileResponse", file);
	}

	createRelativeFolder(file: string) {
		return this.ipc.nodeEvent<string>("createRelativeFolder", "createRelativeFolderResponse", file);
	}

	readRelativeFile(file: string) {
		return this.ipc.nodeEvent<string>("readRelativeFile", "readRelativeFileResponse", file);
	}

	saveRelativeFile(file: string, content: string) {
		return this.ipc.nodeEvent<string>("saveRelativeFile", "saveRelativeFileResponse", file, content);
	}

	saveFileDialog(content: string, file?: string) {
		return this.ipc.nodeEvent<string>("saveFileDialog", "saveFileDialogResponse", content, file);
	}

	openFileDialog(file?: string) {
		return this.ipc.nodeEvent<{ file: string, content: string }>("openFileDialog", "openFileDialogResponse", file);
	}

	saveTempFile(file: string, content: string) {
		return this.saveRelativeFile(path.join(this.tempFolder, file), content);
	}

	saveCodeFile(file: string, content: string) {
		return this.saveTempFile(path.join(this.codeFolder, file), content);
	}

	//*** Complex operations ***

	saveNotebook(originalNotebook: Notebook, forceDialog: boolean = false) {
		if(!originalNotebook.config){
			let notebook = JSON.parse(JSON.stringify(originalNotebook));

			let path = notebook.path;
			notebook.path = undefined;
	
			for (let s of notebook.segments) {
				s.id = undefined;
				s.context = undefined;
			}
	
			return this.saveFileDialog(JSON.stringify(notebook), forceDialog ? undefined : path);

		} else{
			return this.saveRelativeFile(originalNotebook.path, originalNotebook.config);
		}
	}

	openNotebook(context: ContextService, file?: string) {
		return new Promise<Notebook>((resolve, reject) => {
			this.openFileDialog(file).then(data => {
				let notebook: Notebook = JSON.parse(data.content);
				notebook.path = data.file;

				for (let s of notebook.segments) {
					s.id = this.ids.generate().toString();

					if (s.type === 'processor') {
						if (!s.contextName) {
							this.ipc.error("Language error", `Given file is corrupt`);
							reject(null);
							return;
						}

						if (!context.hasContext(s.contextName)) {
							this.ipc.error("Language error", `Language '${s.contextName}' was not found`);
							reject(null);
							return;
						}

						s.context = context.getContext(s.contextName);
					}
				}

				resolve(notebook);

			}).catch(err => {
				reject(null);
			})
		})
	}
}
