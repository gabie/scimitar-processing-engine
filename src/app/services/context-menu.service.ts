import { Injectable } from '@angular/core';
import { ContextMenu } from '../model/ContextMenu';

@Injectable({
	providedIn: 'root'
})
export class ContextMenuService {

	menus: any = {};
	contextMenuPos: { 'top.px': number, 'left.px': number } = { 'top.px': 0, 'left.px': 0 };

	contextMenu: ContextMenu = {
		name: "ctx",
		options: []
	};

	constructor() { }

	menuOpen(menu: string) {
		return this.menus[menu];
	}

	contextMenuOpen() {
		return this.menuOpen('ctx');
	}

	anyTopMenuOpen() {
		return !this.contextMenuOpen() && Object.getOwnPropertyNames(this.menus).length;
	}

	closeContextMenu() {
		this.menus = {};
	}

	openTopMenu(menu: string) {
		let opt = !this.menus[menu];
		this.menus = {};

		if (opt) {
			this.menus[menu] = true;

		} else {
			this.closeContextMenu();
		}

		this.contextMenuPos = {
			'left.px': document.getElementById('ctx_opt_' + menu).getBoundingClientRect().left,
			'top.px': 32,
		}
	}

	moveTopMenu(menu: string) {
		if (this.anyTopMenuOpen()) {
			this.menus = {};
			this.menus[menu] = true;

			this.contextMenuPos = {
				'left.px': document.getElementById('ctx_opt_' + menu).getBoundingClientRect().left,
				'top.px': 32,
			}
		}
	}

	openContextMenu(menu: ContextMenu, mouseX: number, mouseY: number){
		this.contextMenu = menu;

		this.menus = {};
		this.menus['ctx'] = true;
		
		this.contextMenuPos = {
			'left.px': mouseX + 1,
			'top.px': mouseY + 1,
		}
	}
}
