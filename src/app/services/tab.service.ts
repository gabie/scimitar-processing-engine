import { Injectable } from '@angular/core';
import { Notebook } from '../model/Notebook';

@Injectable({
	providedIn: 'root'
})
export class TabService {

	newNotebookRegex: string = '^New notebook\\s?(\\d+)?$';

	notebooks: Notebook[] = [];
	currentNotebook: Notebook;

	constructor() { }

	close(notebook: Notebook = this.currentNotebook){
		if(notebook){
			let index = this.notebooks.indexOf(notebook);

			if(index != -1){
				if(this.notebooks.length == 1){
					this.currentNotebook = null;
				}
	
				this.notebooks.splice(index, 1);
	
				if(this.currentNotebook == notebook){
					if(this.notebooks[index]){
						this.currentNotebook = this.notebooks[index];
					
					} else if(index > 0 && this.notebooks[index - 1]){
						this.currentNotebook = this.notebooks[index - 1];	
					}
				}
			}
		}
	}

	closeAll(){
		this.notebooks = [];
		this.currentNotebook = null;
	}

	newTab(){
		let unnamedNotebooks = this.notebooks.filter(x => new RegExp(this.newNotebookRegex, 'g').test(x.name));
		let index = Math.max(...unnamedNotebooks.map(x => new RegExp(this.newNotebookRegex, 'g').exec(x.name)).map(x => x && x[1]? parseInt(x[1]) : 0));

		this.notebooks.push(new Notebook(`New notebook${unnamedNotebooks.length > 0? ` ${index + 1}` : ''}`, []));

		if(!this.currentNotebook){
			this.currentNotebook = this.notebooks[this.notebooks.length - 1];
		}
	}
}
