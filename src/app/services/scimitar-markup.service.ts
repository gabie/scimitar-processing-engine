import { Injectable } from '@angular/core';
import { FileService } from './file.service';

export class ScimitarResults {
	segments: {
		type: string,
		content: string
	}[];
}

@Injectable({
	providedIn: 'root'
})
export class ScimitarMarkupService {

	validTypes: string[] = ['RAW', 'LATEX', 'IMG'];

	constructor(
		public file: FileService
	) { }

	//Function from https://stackoverflow.com/questions/5717093/check-if-a-javascript-string-is-a-url/43467144
	private hasProtocol(str) {
		return /^[a-z]+:\/\/.+$/.test(str);
	}

	parse(data: string) {
		let res: ScimitarResults;

		try {
			res = JSON.parse(data);

		} catch (err) {
			return null;
		}

		if (!res || !res.segments || !res.segments.length) {
			return null;
		}

		for (let s of res.segments) {
			if (!s.type || !s.content || !this.validTypes.includes(s.type)) {
				return null;
			}

			if(s.type === 'IMG' && !this.hasProtocol(s.content)){ //Content is a path
				this.file.toAbsolutePath(s.content).then(data => {
					s.content = `file://${data}?c=${Math.random()}`;
				})
			}
		}

		return res;
	}

	invalidFormat(): ScimitarResults {
		return {
			segments: [
				{
					type: "RAW",
					content: "Invalid Scimitar Markup result"
				}
			]
		};
	}
}
