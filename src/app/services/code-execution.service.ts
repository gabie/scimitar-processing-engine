import { Injectable } from '@angular/core';
import { IpcService } from './ipc.service';
import { ContextService } from './context.service';
import { LanguageContext } from '../model/LanguageContext';

@Injectable({
	providedIn: 'root'
})
export class CodeExecutionService {

	constructor(
		private ipc: IpcService
	) {}

	executeCode(context: LanguageContext, filename: string){
		return this.ipc.nodeEvent<string>("executeCode", "executeCodeResponse", context, filename);
	}
}
