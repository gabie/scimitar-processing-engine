import { Injectable } from '@angular/core';
import { IpcRenderer } from 'electron';
import { IpcModelResponse } from '../model/IpcModelResponse';

@Injectable({
	providedIn: 'root'
})
export class IpcService {

	private ipc: IpcRenderer;

	maximized: boolean = false;

	constructor() {
		if ((<any>window).require) {
			try {
				this.ipc = (<any>window).require('electron').ipcRenderer;
			} catch (error) {
				throw error;
			}

		} else {
			console.warn('Could not load electron ipc');
		}

		this.ipc.on("maximized", () => {
			this.maximized = true;
		})

		this.ipc.on("unmaximized", () => {
			this.maximized = false;
		})
	}

	nodeEvent<T>(message: string, response: string, ...args): Promise<T>{
		return new Promise<T>((resolve, reject) => {
			this.ipc.once(response, (event, arg: IpcModelResponse<T>) => {
				if(arg && !arg.error){
					resolve(arg.message);

				} else{
					reject(arg.message);
				}
			});

			this.ipc.send(message, ...args);
		});
	}

	send(event: string, ...args) {
		this.ipc.send(event, ...args);
	}

	error(title: string, content: string){
		this.send("showError", title, content);
	}

	info(title: string, content: string){
		this.send("showInfo", title, content);
	}
}
