import { Injectable } from '@angular/core';
import { LanguageContext } from '../model/LanguageContext';
import { IpcService } from './ipc.service';
import { FileService } from './file.service';

@Injectable({
	providedIn: 'root'
})
export class ContextService {

	configPath: string = "config";
	langConfig: string = `${this.configPath}/languages.json`;
	contexts = {};

	constructor(
		private file: FileService
	) {}

	getContexts(){
		return this.file.readRelativeFile(this.langConfig);
	}

	saveContexts(content: string){
		return this.file.saveRelativeFile(this.langConfig, content);
	}

	updateContexts(){
		return this.file.relativeFileExists(this.configPath).then(data => {
			if(data){
				this.file.relativeFileExists(this.langConfig).then(data => {
					if(data){
						this.file.readRelativeFile(this.langConfig).then(file => {
							this.contexts = JSON.parse(file);
						})
					
					} else{
						this.file.saveRelativeFile(this.langConfig, '{}').then(file => {
							this.contexts = {};
						})
					}
		
				}).catch(() => {
					this.file.saveRelativeFile(this.langConfig, '{}').then(file => {
						this.contexts = {};
					})
				})
		
			} else{
				this.file.createRelativeFolder(this.configPath).then(data => {
					this.file.saveRelativeFile(this.langConfig, '{}').then(file => {
						this.contexts = {};
					})
				})
			}
		}).catch(err => {
			this.file.createRelativeFolder(this.configPath).then(data => {
				this.file.saveRelativeFile(this.langConfig, '{}').then(file => {
					this.contexts = {};
				})
			})
		})
	}

	hasContext(name: string): boolean{
		return this.contexts[name];
	}

	getContext(name: string){
		if(!this.contexts[name]){
			throw `Unknown language: '${name}'`;
		}

		return this.contexts[name];
	}

	getAvailableContexts(){
		if(this.contexts){
			let res = [];

			for(let c of Object.getOwnPropertyNames(this.contexts)){
				res.push(c);
			}

			return res;

		} else{
			return [];
		}
	}
}
