import { Injectable } from '@angular/core';
import { ContextService } from './context.service';

@Injectable({
	providedIn: 'root'
})
export class InitService {

	syncPromise: Promise<void>;

	constructor(
		private context: ContextService
	) {
		this.syncPromise = new Promise((resolve, reject) => {
			this.context.updateContexts().then(file => {
				resolve();
			
			}).catch(err => {
				reject("error while reading configuration");
			})
		});
	}

	sync(): Promise<void>{
		return this.syncPromise;
	}
}
