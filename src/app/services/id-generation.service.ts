import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class IdGenerationService {

	idSequence: number = 0;

	constructor() { }

	generate(){
		return this.idSequence ++;
	}
}
