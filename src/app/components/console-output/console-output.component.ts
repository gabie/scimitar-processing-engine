import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'console-output',
	templateUrl: './console-output.component.html',
	styleUrls: ['./console-output.component.scss']
})
export class ConsoleOutputComponent implements OnInit {

	@Input() type: string;
	@Input() content: string;

	constructor() { }

	ngOnInit() {
	}

}
