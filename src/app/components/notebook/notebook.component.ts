import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { LanguageContext } from 'src/app/model/LanguageContext';
import { InitService } from 'src/app/services/init.service';
import { ContextService } from 'src/app/services/context.service';
import { Notebook, NotebookSegment } from 'src/app/model/Notebook';
import { IdGenerationService } from 'src/app/services/id-generation.service';

import * as ace from 'brace';
import 'brace/mode/json';

import 'brace/theme/monokai';
import 'brace/theme/eclipse';
import { FileService } from 'src/app/services/file.service';

@Component({
	selector: 'notebook',
	templateUrl: './notebook.component.html',
	styleUrls: ['./notebook.component.scss']
})
export class NotebookComponent implements AfterViewInit {

	@Input() notebook: Notebook;

	availableContexts: string[] = [];

	editor: ace.Editor;
	theme: string = 'ace/theme/monokai';

	constructor(
		private init: InitService,
		private ids: IdGenerationService,
		private file: FileService,
		private context: ContextService
	) {
		this.init.sync().then(() => {
			this.availableContexts = this.context.getAvailableContexts();
		});
	}

	ngAfterViewInit() {
		if (this.notebook.config) {
			this.editor = ace.edit(`code_${this.notebook.path}`);
			this.editor.$blockScrolling = Infinity;

			this.editor.setTheme(this.theme);
			this.editor.getSession().setMode('ace/mode/json');
			this.editor.setValue(this.notebook.config, 1);

			this.editor.setOptions({
				maxLines: Infinity,
				fontSize: '100%'
			});

			this.editor.on("change", () => {
				this.notebook.config = this.editor.getValue();
			});

			this.editor.commands.addCommand({
				name: 'Save',
				exec: () => this.file.saveNotebook(this.notebook),
				bindKey: 'Ctrl+s'
			});
		}
	}

	addSegment(type: string, index: number) {
		if (type === 'processor') {
			this.addProcessor(index);
		}

		if (type === 'text') {
			this.addText(index);
		}
	}

	addProcessor(insertionIndex: number = this.notebook.segments.length) {
		let segment = new NotebookSegment();
		segment.id = this.notebook.name + "_" + this.ids.generate();
		segment.type = 'processor';

		let index = -1;

		for (let i = insertionIndex - 1; i >= 0; i--) {
			if (this.notebook.segments[i].type === 'processor') {
				index = i;
				break;
			}
		}

		if (index != -1) {
			segment.contextName = this.notebook.segments[index].contextName;
			segment.context = this.notebook.segments[index].context;

		} else {
			segment.contextName = this.availableContexts[0];
			segment.context = this.context.getContext(this.availableContexts[0]);
		}

		this.notebook.segments.splice(insertionIndex, 0, segment);
	}

	addText(insertionIndex: number = this.notebook.segments.length) {
		let segment = new NotebookSegment();
		segment.id = this.notebook.name + "_" + this.ids.generate();
		segment.type = 'text';
		segment.code = '';

		this.notebook.segments.splice(insertionIndex, 0, segment);
	}

	deleteSegment(segment: NotebookSegment) {
		let index = this.notebook.segments.indexOf(segment);

		if (index != -1) {
			this.notebook.segments.splice(index, 1);
		}
	}
}
