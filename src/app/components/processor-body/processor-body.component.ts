import { Component, OnInit } from '@angular/core';
import { Notebook } from 'src/app/model/Notebook';
import { TabService } from 'src/app/services/tab.service';
import { ContextMenuService } from 'src/app/services/context-menu.service';

@Component({
	selector: 'app-processor-body',
	templateUrl: './processor-body.component.html',
	styleUrls: ['./processor-body.component.scss']
})
export class ProcessorBodyComponent implements OnInit {

	constructor(
		public tabs: TabService,
		public ctx: ContextMenuService,
	) {}

	ngOnInit(){}

	openContextMenu($event, notebook: Notebook){
		let mouseX = $event.pageX;
		let mouseY = $event.pageY;

		this.ctx.openContextMenu({
			name: "ctx",
			options: [
				{
					text: 'Close',
					annotation: "Ctrl+W",
					action: () => this.tabs.close(notebook)
				},
				{
					text: 'Close Others',
					action: () => {
						this.tabs.currentNotebook = notebook;
						this.tabs.notebooks = [notebook];
					}
				},
				{
					text: 'Close all',
					annotation: "Ctrl+Shift+W",
					action: () => this.tabs.closeAll()
				}
			]
		}, mouseX, mouseY);

		$event.stopPropagation();
	}

	openHeaderContextMenu($event){
		let mouseX = $event.pageX;
		let mouseY = $event.pageY;

		this.ctx.openContextMenu({
			name: "ctx",
			options: [
				{
					text: 'New Tab',
					annotation: "Ctrl+N",
					action: () => this.tabs.newTab()
				}
			]
		}, mouseX, mouseY);
	}
}
