import { Component, OnInit, Input } from '@angular/core';
import { ContextMenu, ContextMenuOption } from 'src/app/model/ContextMenu';
import { ContextMenuService } from 'src/app/services/context-menu.service';

@Component({
	selector: 'context-menu',
	templateUrl: './context-menu.component.html',
	styleUrls: ['./context-menu.component.scss']
})
export class ContextMenuComponent implements OnInit {

	@Input() menu: ContextMenu;
	@Input() isTopMenu: boolean = true;

	constructor(
		public ctx: ContextMenuService
	) { }

	ngOnInit() {
	}

	doAction(option: ContextMenuOption){
		if(option && option.action){
			option.action();
			this.ctx.closeContextMenu();
		}
	}

	isInvalid(option: ContextMenuOption){
		return option.validation && !option.validation();
	}
}
