import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotebookSegment } from 'src/app/model/Notebook';
import { ContextMenuService } from 'src/app/services/context-menu.service';

@Component({
	selector: 'text-engine',
	templateUrl: './text-engine.component.html',
	styleUrls: ['./text-engine.component.scss']
})
export class TextEngineComponent implements OnInit {

	@Input() segment: NotebookSegment;

	@Output() segmentDeletion: EventEmitter<void> = new EventEmitter<void>();
	@Output() addSegment: EventEmitter<string> = new EventEmitter<string>();

	editing: boolean = true;

	constructor(
		private ctx: ContextMenuService
	) { }

	ngOnInit() {
		if(!this.segment.code){
			this.focusTextArea();
	
		} else{
			this.editing = false;
		}
	}

	focusTextArea(){
		setTimeout(() => {
			document.getElementById('editionArea').focus();
		}, 100);
	}

	enableEditMode(){
		this.editing = true; 
		this.focusTextArea();
	}

	disableEditMode(){
		this.editing = false;

		if(!this.segment.code || !this.segment.code.length){
			this.segmentDeletion.emit();
		}
	}

	openContextualMenu($event){
		let mouseX = $event.pageX;
		let mouseY = $event.pageY;

		this.ctx.openContextMenu({
			name: 'ctx',
			options: [
				{
					text: 'Insert processor above',
					action: () => this.addSegment.emit('processor')
				},
				{
					text: 'Insert text above',
					action: () => this.addSegment.emit('text')
				},
				{
					text: 'Delete text',
					action: () => this.segmentDeletion.emit()
				}
			]
		}, mouseX, mouseY);
	}
}
