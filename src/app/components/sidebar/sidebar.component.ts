import { Component, OnInit } from '@angular/core';
import { TabService } from 'src/app/services/tab.service';
import { Notebook } from 'src/app/model/Notebook';

@Component({
	selector: 'sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

	fileSection: boolean = true;
	openNotebooksSection: boolean = true;

	constructor(
		public tabs: TabService
	) { }

	ngOnInit() {
	}
}
