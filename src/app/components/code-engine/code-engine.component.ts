import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';

import * as ace from 'brace';

import 'brace/mode/python';
import 'brace/mode/haskell';
import 'brace/mode/ulan';

import 'brace/theme/monokai';
import 'brace/theme/eclipse';

import { CodeExecutionService } from 'src/app/services/code-execution.service';
import { FileService } from 'src/app/services/file.service';
import { ContextService } from 'src/app/services/context.service';
import { NotebookSegment } from 'src/app/model/Notebook';
import { ContextMenuService } from 'src/app/services/context-menu.service';
import { ContextMenu } from 'src/app/model/ContextMenu';
import { ScimitarMarkupService, ScimitarResults } from 'src/app/services/scimitar-markup.service';

type OutputType = "AUTO" | "RAW" | "LATEX" | "LOADING" | "ERROR";

@Component({
	selector: 'code-engine',
	templateUrl: './code-engine.component.html',
	styleUrls: ['./code-engine.component.scss']
})
export class CodeEngineComponent implements OnInit, AfterViewInit {

	@Input() segment: NotebookSegment;

	@Input() theme: string = 'ace/theme/monokai';
	@Input() availableContexts: string[] = [];

	@Output() segmentDeletion: EventEmitter<void> = new EventEmitter<void>();
	@Output() addSegment: EventEmitter<string> = new EventEmitter<string>();

	editor: ace.Editor;

	prevType: OutputType = "LATEX";
	type: OutputType = "LATEX";

	consoleOutput: string = '';
	parsedConsoleOutput: ScimitarResults;

	contextMenu: ContextMenu = {
		name: "ctx",
		options: [
			{
				text: 'Execute',
				annotation: "Ctrl+B",
				action: () => this.executeCode(),
				validation: () => this.editor.getValue().length
			},
			{
				separator: true
			},
			{
				text: 'Clear code',
				action: () => this.editor.setValue('', 1),
				validation: () => this.editor.getValue().length
			},
			{
				text: 'Example for this language',
				action: () => this.editor.setValue(this.segment.context.sampleCode, 1),
				validation: () => this.segment.context.sampleCode
			},
			{
				separator: true
			},
			{
				text: 'Insert processor above',
				action: () => this.addSegment.emit('processor')
			},
			{
				text: 'Insert text above',
				action: () => this.addSegment.emit('text')
			},
			{
				text: 'Delete processor',
				action: () => this.deleteSegment()
			}
		]
	};

	constructor(
		public file: FileService,
		public context: ContextService,
		public codeExecution: CodeExecutionService,
		public markup: ScimitarMarkupService,
		public ctx: ContextMenuService,
	) { }

	ngOnInit() { }

	ngAfterViewInit() {
		this.editor = ace.edit(`code_${this.segment.id}`);
		
		this.editor.$blockScrolling = Infinity;

		if(this.segment.context.syntax){
			this.editor.getSession().setMode(this.segment.context.syntax);
		}

		this.editor.setTheme(this.theme);

		this.editor.setOptions({
			maxLines: Infinity,
			fontSize: '100%'
		});

		this.editor.on("change", () => {
			this.segment.code = this.editor.getValue();
		});

		this.editor.setValue(this.segment && this.segment.code? this.segment.code : '', 1);

		this.editor.commands.addCommand({
			name: 'Execute',
			exec: () => this.executeCode(),
			bindKey: 'Ctrl+b'
		});
	}

	openContextMenu($event){
		let mouseX = $event.pageX;
		let mouseY = $event.pageY;

		this.ctx.openContextMenu(this.contextMenu, mouseX, mouseY);
	}

	executeCode(){
		if (this.type != 'LOADING') {
			this.clearErrorMarkers();

			if (this.type != "ERROR") {
				this.prevType = this.type;
			}

			this.file.saveCodeFile('code' + this.segment.context.extension, this.editor.getValue()).then((filename) => {
				this.type = 'LOADING';

				this.codeExecution.executeCode(this.segment.context, filename).then(out => {
					this.consoleOutput = out;
					this.parsedConsoleOutput = this.markup.parse(out);

					if(this.parsedConsoleOutput){
						this.prevType = "AUTO";

					} else{
						this.parsedConsoleOutput = this.markup.invalidFormat();
					}
					
					if(this.segment.context.error && this.segment.context.error.regex){
						const regex = new RegExp(this.segment.context.error.regex);

						if(regex.test(this.consoleOutput)){
							this.type = 'ERROR';
							
							if(this.segment.context.error.lineCapture){
								let res = regex.exec(this.consoleOutput);

								if(res[this.segment.context.error.lineCapture]){
									this.setErrorMarker(parseInt(res[this.segment.context.error.lineCapture]) - 1);
								}
							}
					
						} else{
							this.type = this.prevType;
						}		

					} else{
						this.type = this.prevType;
					}	


				}).catch(err => {
					this.consoleOutput = err && err !== ''? err : "Error while executing code";
					this.type = "ERROR";
				})

			}).catch(err => {
				this.consoleOutput = err && err !== ''? err : "Error while executing code";
				this.type = "ERROR";
			})
		}
	}

	clearErrorMarkers(){
		this.editor.getSession().setAnnotations([]);
	}

	setErrorMarker(line: number){
		this.editor.getSession().setAnnotations([
			{
				row: line,
				column: 0,
				type: "error",
				text: ""
			}
		]);
	}

	changeContext($event){
		this.segment.contextName = $event.target.value;
		this.segment.context = this.context.getContext($event.target.value);

		if(this.segment.context.syntax){
			this.editor.getSession().setMode(this.segment.context.syntax);
		}
	}

	deleteSegment(){
		this.segmentDeletion.emit();
	}
}
