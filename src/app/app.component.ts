import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { IpcService } from './services/ipc.service';
import { TabService } from './services/tab.service';
import { ContextMenuService } from './services/context-menu.service';
import { TopMenuOption } from './model/ContextMenu';
import { FileService } from './services/file.service';
import { ContextService } from './services/context.service';
import { InitService } from './services/init.service';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { Notebook } from './model/Notebook';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss']
})
export class AppComponent {

	topMenuOptions: TopMenuOption[] = [
		{
			text: 'File',
			menu: {
				name: 'file',
				options: [
					{
						text: 'New notebook',
						annotation: 'Ctrl+N',
						action: () => this.tabs.newTab()
					},
					{
						text: 'Open notebook',
						annotation: 'Ctrl+O',
						action: () => this.openNotebook(),
					},
					{
						separator: true
					},
					{
						text: 'Save',
						annotation: 'Ctrl+S',
						action: () => this.file.saveNotebook(this.tabs.currentNotebook).catch(() => { }),
						validation: () => this.tabs.currentNotebook
					},
					{
						text: 'Save as',
						annotation: 'Ctrl+Shift+S',
						action: () => this.file.saveNotebook(this.tabs.currentNotebook, true).catch(() => { }),
						validation: () => this.tabs.currentNotebook && !this.tabs.currentNotebook.config
					},
					{
						separator: true
					},
					{
						text: 'Close current notebook',
						annotation: 'Ctrl+W',
						action: () => this.tabs.close(),
						validation: () => this.tabs.currentNotebook
					},
					{
						text: 'Close all',
						annotation: 'Ctrl+Shift+W',
						action: () => this.tabs.closeAll(),
						validation: () => this.tabs.notebooks && this.tabs.notebooks.length
					}
				]
			}
		},
		{
			text: 'Preferences',
			menu: {
				name: 'pref',
				options: [
					{
						text: 'Languages',
						action: () => {
							this.context.getContexts().then(data => {
								let notebook = this.tabs.notebooks.find(x => x.path === this.context.langConfig);

								if(!notebook){
									let notebook: Notebook = {
										name: 'Languages',
										segments: [],
										path: this.context.langConfig,
										config: data
									};
	
									this.tabs.notebooks.push(notebook);
									this.tabs.currentNotebook = notebook;
						
								} else{
									this.tabs.currentNotebook = notebook;
								}
							})
						}
					},
					{
						text: 'Appearance'
					},
					{
						text: 'General'
					}
				]
			}
		},
		{
			text: 'Help',
			menu: {
				name: 'help',
				options: [
					{
						text: 'Manual'
					},
					{
						text: 'Changelog'
					},
					{
						separator: true
					},
					{
						text: 'About Scimitar'
					},
					{
						text: 'About us'
					}
				]
			}
		},
	];

	constructor(
		private ipc: IpcService,
		public tabs: TabService,
		public file: FileService,
		public context: ContextService,
		public init: InitService,
		private hotkeys: HotkeysService,
		public ctx: ContextMenuService
	) {
		this.hotkeys.add(new Hotkey('ctrl+n', (event: KeyboardEvent): boolean => {
			this.tabs.newTab();
			return false;
		}));
		
		this.hotkeys.add(new Hotkey('ctrl+o', (event: KeyboardEvent): boolean => {
			this.openNotebook();
			return false;
		}));
		
		this.hotkeys.add(new Hotkey('ctrl+s', (event: KeyboardEvent): boolean => {
			this.file.saveNotebook(this.tabs.currentNotebook).catch(() => { });
			return false;
		}));
		
		this.hotkeys.add(new Hotkey('ctrl+shift+s', (event: KeyboardEvent): boolean => {
			if(!this.tabs.currentNotebook.config){
				this.file.saveNotebook(this.tabs.currentNotebook, true).catch(() => { });
			}

			return false;
		}));
		
		this.hotkeys.add(new Hotkey('ctrl+w', (event: KeyboardEvent): boolean => {
			this.tabs.close();
			return false;
		}));
		
		this.hotkeys.add(new Hotkey('ctrl+shift+w', (event: KeyboardEvent): boolean => {
			this.tabs.closeAll();
			return false;
		}));
	}

	minimizeWindow() {
		this.ipc.send('minimizeWindow');
	}

	maximizeWindow() {
		this.ipc.send('maximizeWindow');
	}

	closeWindow() {
		this.ipc.send('closeWindow');
	}

	isMaximized() {
		return this.ipc.maximized;
	}

	openNotebook() {
		this.init.sync().then(() => this.file.openNotebook(this.context).then(notebook => {
			this.tabs.notebooks.push(notebook);
			this.tabs.currentNotebook = notebook;

		}).catch(() => { }));
	}
}
