ace.define("ace/mode/ulan_highlight_rules", ["require", "exports", "module", "ace/lib/oop", "ace/mode/text_highlight_rules"], function (acequire, exports, module) {
	"use strict";

	var oop = acequire("../lib/oop");
	var TextHighlightRules = acequire("./text_highlight_rules").TextHighlightRules;

	var UlanHighlightRules = function () {

		var keywords = (
			"If|Else|For|Parallel|While|Break|" +
			"Function|Operator|Operation|Class|Cast|Syntax|" +
			"Full|Left|Right|Infix|Prefix|Suffix|Prec|" +
			"As|Define|For|From|In|Lock|Return|Yield|Const|Is|"
		);

		var containers = (
			"Vector|Deque|Set|List|SortedVector|SortedDeque|SortedSet|List"
		);

		var builtinConstants = (
			"True|False"
		);

		var keywordMapper = this.createKeywordMapper({
			"constant.language": builtinConstants,
			"keyword": keywords,
			"support.class": containers
		}, "identifier", true);

		var identifierRegex = "[a-zA-Z_\u03B1-\u03C9\u0391-\u03A9]\\w*";
		var numberRegex = "(-|\\b)[0-9]{1,}(\\.[0-9]{1,})?\\b";

		this.$rules = {
			"start": [{
				token: ["keyword", "text", "support.class"],
				regex: "(Module)(\\s+)(" + identifierRegex + ")"
			},{
				token: ["keyword", "text", "support.class", "text", "keyword"],
				regex: "(Using)(\\s+)(" + identifierRegex + ")(\\s+)(syntax)"
			},{
				token: ["keyword", "text", "keyword.operator", "support.class", "keyword.operator"],
				regex: "(Import)(\\s*)(<)(" + identifierRegex + ")(>)"
			},{
				token: ["keyword", "text", "keyword", "text", "variable.function"],
				regex: "([Dd][Ee][Ff][Ii][Nn][Ee])(\\s+)([Ff][Uu][Nn][Cc][Tt][Ii][Oo][Nn])(\\s+)(" + identifierRegex + ")(?=\\s*\\()"
			},{
				token: ["variable.parameter", "text", "keyword.operator", "text", "support.class", "keyword.operator", "identifier"],
				regex: "(" + identifierRegex + ")(\\s*)(:)(\\s*)(" + identifierRegex + ")(&{0,2})(?=\\s*(\\)|\\]|,))"
			},{
				token: ["identifier", "text", "keyword.operator", "text", "support.class", "keyword.operator"],
				regex: "(" + identifierRegex + ")(\\s*)(:)(\\s*)(" + identifierRegex + ")(&{0,2})"
			},{
				token: "comment",
				regex: "//.*$"
			}, {
				token: "string",
				regex: '"(?=.)',
				next: "qqstring"
			}, {
				token: "constant.numeric",
				regex: numberRegex
			}, {
				token: keywordMapper,
				regex: "" + identifierRegex + "\\b"
			}, {
				token: "keyword.operator",
				regex: "=|\\+=|-=|\\*=|/=|\\^=|%=|:=|==|!=|<|>|<=|>=|\\+|-|\\*|/|\\^|%|&&|\\|\\||->|<->|!|&|\\.\\.\\.|::|:"
			}, {
				token: "paren.lparen",
				regex: "[\\[\\(\\{]"
			}, {
				token: "paren.rparen",
				regex: "[\\]\\)\\}]"
			}, {
				token: "text",
				regex: "\\s+"
			}],
			"qqstring": [{
				token: "string",
				regex: "\\\\$",
				next: "qqstring"
			}, {
				token: "string",
				regex: '"|$',
				next: "start"
			}, {
				defaultToken: "string"
			}]
		};
	};

	oop.inherits(UlanHighlightRules, TextHighlightRules);

	exports.UlanHighlightRules = UlanHighlightRules;
});

ace.define("ace/mode/folding/pythonic", ["require", "exports", "module", "ace/lib/oop", "ace/mode/folding/fold_mode"], function (acequire, exports, module) {
	"use strict";

	var oop = acequire("../../lib/oop");
	var BaseFoldMode = acequire("./fold_mode").FoldMode;

	var FoldMode = exports.FoldMode = function (markers) {
		this.foldingStartMarker = new RegExp("([\\[{])(?:\\s*)$|(" + markers + ")(?:\\s*)(?:#.*)?$");
	};
	oop.inherits(FoldMode, BaseFoldMode);

	(function () {

		this.getFoldWidgetRange = function (session, foldStyle, row) {
			var line = session.getLine(row);
			var match = line.match(this.foldingStartMarker);
			if (match) {
				if (match[1])
					return this.openingBracketBlock(session, match[1], row, match.index);
				if (match[2])
					return this.indentationBlock(session, row, match.index + match[2].length);
				return this.indentationBlock(session, row);
			}
		};

	}).call(FoldMode.prototype);

});

ace.define("ace/mode/ulan", ["require", "exports", "module", "ace/lib/oop", "ace/mode/text", "ace/mode/ulan_highlight_rules", "ace/range"], function (acequire, exports, module) {
	"use strict";

	var oop = acequire("../lib/oop");
	var TextMode = acequire("./text").Mode;
	var UlanHighlightRules = acequire("./ulan_highlight_rules").UlanHighlightRules;
	var PythonFoldMode = acequire("./folding/pythonic").FoldMode;
	var Range = acequire("../range").Range;

	function ci(str){
		let res = "";

		for(let c of str){
			res += "[" + c.toLowerCase() + c.toUpperCase() + "]";
		}

		return res;
	}

	var Mode = function () {
		this.HighlightRules = UlanHighlightRules;
		this.foldingRules = new PythonFoldMode(`${ci('as')}|${ci('if')}.*|${ci('else')}.*|${ci('while')}.*|${ci('for')}.*`);
		this.$behaviour = this.$defaultBehaviour;
	};
	oop.inherits(Mode, TextMode);

	(function () {

		this.lineCommentStart = "//";

		this.getNextLineIndent = function (state, line, tab) {
			var indent = this.$getIndent(line);

			var tokenizedLine = this.getTokenizer().getLineTokens(line, state);
			var tokens = tokenizedLine.tokens;

			if (tokens.length && tokens[tokens.length - 1].type == "comment") {
				return indent;
			}

			if (state == "start") {
				var match = line.match(/^.*([\{\(\[]|as)\s*$/);
				if (match) {
					indent += tab;
				}
			}

			return indent;
		};

		this.$id = "ace/mode/ulan";
	}).call(Mode.prototype);

	exports.Mode = Mode;
});
