# Scimitar Processing Engine

## About the project

Scimitar is an environment that allows the use of multiple programming languages and the visualization of the results. This project aims to be an easily expandable notebook-based 
development tool useful for both enthusiasts and professionals who are looking for a lightweight alternative to other famous tools like Jupyter. Adding support for a new language, 
a basic linter or displaying data should be as simple as editing a JSON file and executing a print statement. This is Scimitar's philosophy and that is how its development will continue.

## Configuration

Scimitar aims to be as easy to use as possible, so only one configuration file is strictly needed. This file is called "languages.json" and tells the program how to run the code you enter.
This file can be edited from the program itself and it will be automatically created. Lets see a quick example for Python on Windows:

```json
...
    "python": {
        "extension": ".py",
        "syntax": "ace/mode/python",

        "error": {
            "regex": "File .+?, line (\\d+).*",
            "lineCapture": 1
        },

        "execution":{
            "command": "AbsolutePathToYourPythonInterpreter/python.exe",
            "args":[
                "{{file}}"
            ]
        },

        "sampleCode": "x = 0\n\nfor i in range(100):\n    x += i\n\nprint(\"x = {}\".format(x))"
    }
...
```
Let's explain this snippet line by line:
* **extension:** this is a self explanatory one. It expects the file format for the language you are configuring
* **syntax:** since Ace Editor is used internally, "ace/mode/{language}" is the typical standard name for syntaxes. As you could imagine, this option will allow syntax highlighting for the language you are configuring if Ace supports it.
* **error:** this options enables the optional linting capabilities. It expects a Javascript regex and the index of the capture group that contains the line with the error. Most languages have standard formats for syntax errors and unexpected exceptions, so this treatment is often possible.
* **execution:** this is the most important one. it expects an executable file (the compiler/interpreter for the language) and a series of arguments. When you append the arguments to the executable file, the code contained in "{{file}}" (this will be automatically substituted with a temporary file that contains the code) should be run, so it is the programmer's task to construct this command as efficiently as possible.
* **sampleCode:** while not as important, sample code snippets for your languages are a good practise and allow fast testing when specified. This only serves the purpose of not having to type test code when developing an extension.

Once you configure your languages.json, you are all set! Restart Scimitar and your new language will be automatically detected in your notebooks.

## Usage

Now you can execute code snippets and you can visualize raw outputs from the console, but you might want to automatically use LaTeX, show images or event show different outputs in one single run. This is accomplished using what it's called **Scimitar Markup**.
This is no more than a fancy name for a short JSON file with a specific syntax. Let's see an example:

```json
{
    "segments": [
        {
            "type": "RAW",
            "content": "Test content"
        },
        {
            "type": "LATEX",
            "content": "x=y^2-2"
        },
        {
            "type": "IMG",
            "content": "resources/temp/testImage.bmp"
        }
    ]
}
```
Each element in the "segments" array is a row in the output shown in Scimitar. The allowed types are the following:
* **RAW:** as the name says, it shows the unformatted raw output of the console.
* **LATEX:** it renders its content as LaTeX. A syntax error would result in an empty output.
* **IMG:** it renders an image. This can be either a URL or a path. It is advised that the path "resources/temp" is used when both saving images and displaying them for simplicity.

## Project Status

Scimitar is still in early development, but it definitely is usable. You can download the current release and use it for free, but be aware that this is a free project and no warranty is given whatsoever. If you spot a bug or have a cool suggestion you can create an issue freely and I will respond as soon as I can. Please, be aware that this is mantained by only one programmer with no external funding, so expect things to go slowly. Please, be patient! :D