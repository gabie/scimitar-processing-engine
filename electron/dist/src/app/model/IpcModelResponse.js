"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IpcModelResponse = /** @class */ (function () {
    function IpcModelResponse(_message, _error) {
        if (_error === void 0) { _error = false; }
        this.message = _message;
        this.error = _error;
    }
    return IpcModelResponse;
}());
exports.IpcModelResponse = IpcModelResponse;
//# sourceMappingURL=IpcModelResponse.js.map