"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IpcModelResponse_1 = require("../src/app/model/IpcModelResponse");
var electron_1 = require("electron");
var path = require("path");
var url = require("url");
var win;
electron_1.app.on('ready', createWindow);
electron_1.app.on('activate', function () {
    if (win === null) {
        createWindow();
    }
});
electron_1.app.on('window-all-closed', function () {
    if (process.platform !== 'darwin')
        electron_1.app.quit();
});
function createWindow() {
    win = new electron_1.BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
    win.maximize();
    win.loadURL(url.format({
        pathname: path.join(__dirname, "/../../dist/index.html"),
        protocol: 'file:',
        slashes: true,
    }));
    win.webContents.openDevTools();
    win.on('closed', function () {
        win = null;
    });
}
//*** FILESYSTEM ***
electron_1.ipcMain.on('saveFile', function (event, file, content) {
    console.log(file, content);
    //fs.writeFileSync(arg[0], arg[1]);
    console.log(new IpcModelResponse_1.IpcModelResponse("done"));
    win.webContents.send('saveFileResponse', new IpcModelResponse_1.IpcModelResponse("done"));
});
//# sourceMappingURL=main.js.map