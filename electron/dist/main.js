"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var electron_1 = require("electron");
var child_process = require("child_process");
var path = require("path");
var url = require("url");
var fs = require("fs");
var win;
electron_1.app.on('ready', createWindow);
electron_1.app.on('activate', function () {
    if (win === null) {
        createWindow();
    }
});
electron_1.app.on('window-all-closed', function () {
    if (process.platform !== 'darwin')
        electron_1.app.quit();
});
function createWindow() {
    win = new electron_1.BrowserWindow({
        frame: false,
        width: 1200,
        height: 900,
        minWidth: 1200,
        minHeight: 900,
        webPreferences: {
            nodeIntegration: true
        },
        icon: path.join(__dirname, "/../../src/assets/icons/scimitar.ico")
    });
    win.setMenu(null);
    win.loadURL(url.format({
        pathname: path.join(__dirname, "/../../dist/index.html"),
        protocol: 'file:',
        slashes: true,
    }));
    win.webContents.openDevTools();
    win.on('closed', function () {
        win = null;
    });
    win.on("maximize", function () {
        win.webContents.send("maximized");
    });
    win.on("unmaximize", function () {
        win.webContents.send("unmaximized");
    });
}
//*** WINDOW CONTROLS ***
electron_1.ipcMain.on("closeWindow", function () {
    win.close();
});
electron_1.ipcMain.on("minimizeWindow", function () {
    win.minimize();
});
electron_1.ipcMain.on("maximizeWindow", function () {
    if (!win.isMaximized()) {
        win.maximize();
    }
    else {
        win.unmaximize();
    }
});
//*** FILESYSTEM ***
electron_1.ipcMain.on('saveRelativeFile', function (event, file, content) {
    fs.writeFileSync(path.join(__dirname, file), content);
    win.webContents.send('saveRelativeFileResponse', {
        error: false,
        message: file
    });
});
electron_1.ipcMain.on('readRelativeFile', function (event, file) {
    var content = fs.readFileSync(path.join(__dirname, file));
    win.webContents.send('readRelativeFileResponse', {
        error: false,
        message: content.toLocaleString()
    });
});
function substitute(str, substitutions) {
    if (substitutions) {
        for (var _i = 0, substitutions_1 = substitutions; _i < substitutions_1.length; _i++) {
            var s = substitutions_1[_i];
            str = str.replace(new RegExp("\\{\\{" + s.from + "\\}\\}", 'g'), s.to);
        }
    }
    return str.toString();
}
function getCommand(execution, substitutions) {
    return [execution.command].concat(execution.args).map(function (p) {
        p = substitute(p, substitutions);
        return p.includes(' ') && !p.startsWith("'") ? "\"" + p + "\"" : p;
    }).join(' ');
}
electron_1.ipcMain.on('executeCode', function (event, context, filename) {
    var substitutions = [
        {
            from: "file",
            to: path.join(__dirname, filename)
        }
    ];
    var out = '';
    var process = child_process.exec(getCommand(context.execution, substitutions));
    process.stdout.on('data', function (data) {
        out += data;
    });
    process.stderr.on('data', function (data) {
        out += data;
    });
    process.on('exit', function (code) {
        win.webContents.send('executeCodeResponse', {
            error: false,
            message: out
        });
    });
    process.on('error', function (err) {
        win.webContents.send('executeCodeResponse', {
            error: true,
            message: 'Error while executing code: ' + err
        });
    });
});
electron_1.ipcMain.on('saveFileDialog', function (event, content, file) {
    if (!file) {
        file = electron_1.dialog.showSaveDialogSync(win, {
            filters: [
                {
                    name: "Scimitar Notebook",
                    extensions: ['scn']
                }
            ]
        });
    }
    if (!file) {
        win.webContents.send('saveFileDialogResponse', {
            error: true,
            message: "No file",
        });
    }
    else {
        fs.writeFileSync(file, content);
        win.webContents.send('saveFileDialogResponse', {
            error: false,
            message: file
        });
    }
});
electron_1.ipcMain.on('openFileDialog', function (event, file) {
    if (!file) {
        file = electron_1.dialog.showOpenDialogSync(win, {
            properties: ['openFile'],
            filters: [
                {
                    name: "Scimitar Notebook",
                    extensions: ['scn']
                }
            ]
        });
    }
    if (!file || !file.length) {
        win.webContents.send('openFileDialogResponse', {
            error: true,
            message: "No file",
        });
    }
    else {
        var content = fs.readFileSync(file[0]);
        win.webContents.send('openFileDialogResponse', {
            error: false,
            message: {
                file: file[0],
                content: content
            }
        });
    }
});
electron_1.ipcMain.on('showError', function (event, title, content) {
    electron_1.dialog.showErrorBox(title, content);
});
electron_1.ipcMain.on('showInfo', function (event, title, content) {
    electron_1.dialog.showMessageBoxSync(win, {
        title: title,
        message: content
    });
});
//# sourceMappingURL=main.js.map