import { app, BrowserWindow, ipcMain, dialog, webContents } from 'electron'

import * as child_process from 'child_process'

import * as path from 'path'
import * as url from 'url'
import * as fs from 'fs'

let win: BrowserWindow;

app.on('ready', createWindow)

app.on('activate', () => {
	if (win === null) {
		createWindow()
	}
})

app.on('window-all-closed', function () {
	if (process.platform !== 'darwin') app.quit()
})

function createWindow() {
	win = new BrowserWindow({
		frame: false,

		width: 1200,
		height: 900,
		minWidth: 1200,
		minHeight: 900,
		
		webPreferences: {
			nodeIntegration: true
		},

		icon: path.join(__dirname, `/../../src/assets/icons/scimitar.ico`)
	});

	win.setMenu(null);

	win.loadURL(
		url.format({
			pathname: path.join(__dirname, `/../../dist/index.html`),
			protocol: 'file:',
			slashes: true,
		})
	);

	win.webContents.openDevTools();

	win.on('closed', () => {
		win = null
	});

	win.on("maximize", () => {
		win.webContents.send("maximized");
	})

	win.on("unmaximize", () => {
		win.webContents.send("unmaximized");
	})
}

//*** WINDOW CONTROLS ***

ipcMain.on("closeWindow", ()=> {
	win.close();
})

ipcMain.on("minimizeWindow", ()=> {
	win.minimize();
})

ipcMain.on("maximizeWindow", ()=> {
	if(!win.isMaximized()){
		win.maximize();
	
	} else{
		win.unmaximize();
	}
})

//*** FILESYSTEM ***

ipcMain.on('toAbsolutePath', (event, file) => {
	if(!path.isAbsolute(file)){
		let projectBase = app.getPath('exe').substring(0, app.getPath('exe').lastIndexOf('\\'));
		file = path.join(projectBase, file);
	}
	
	win.webContents.send('toAbsolutePathResponse', {
		error: false,
		message: file
	});
})

ipcMain.on('existsRelativeFile', (event, file) => {
	try{
		win.webContents.send('existsRelativeFileResponse', {
			error: false,
			message: fs.existsSync(path.join(app.getAppPath(), file))
		});

	} catch(err){
		win.webContents.send('existsRelativeFileResponse', {
			error: true,
			message: false
		});
	}
})

ipcMain.on('createRelativeFolder', (event, file) => {
	fs.mkdirSync(path.join(app.getAppPath(), file));

	win.webContents.send('createRelativeFolderResponse', {
		error: false,
		message: file
	});
})

ipcMain.on('saveRelativeFile', (event, file, content) => {
	fs.writeFileSync(path.join(app.getAppPath(), file), content);

	win.webContents.send('saveRelativeFileResponse', {
		error: false,
		message: file
	});
})

ipcMain.on('readRelativeFile', (event, file) => {
	let content = fs.readFileSync(path.join(app.getAppPath(), file));

	win.webContents.send('readRelativeFileResponse', {
		error: false,
		message: content.toLocaleString()
	});
})

function substitute(str: string, substitutions): string{
	if(substitutions){
		for(let s of substitutions){
			str = str.replace(new RegExp(`\\{\\{${s.from}\\}\\}`, 'g'), s.to);
		}
	}

	return str.toString();
}

function getCommand(execution, substitutions){
	return [execution.command, ...execution.args].map(p => {
		p = substitute(p, substitutions);
		return p.includes(' ') && !p.startsWith("'")? `"${p}"` : p;

	}).join(' ');
}

ipcMain.on('executeCode', (event, context, filename) => {
	let substitutions = [
		{
			from: "file",
			to: path.join(app.getAppPath(), filename)
		}
	]

	let out: string = '';
	let process = child_process.exec(getCommand(context.execution, substitutions));

	process.stdout.on('data', data => {
		out += data;
	});

	process.stderr.on('data', data => {
		out += data;
	});

	process.on('exit', (code) => {
		win.webContents.send('executeCodeResponse', {
			error: false,
			message: out
		});
	})	
	
	process.on('error', (err) => {
		win.webContents.send('executeCodeResponse', {
			error: true,
			message: 'Error while executing code: ' + err
		});
	})
})

ipcMain.on('saveFileDialog', (event, content, file) => {
	if(!file){
		file = dialog.showSaveDialogSync(win, {
			filters: [
				{
					name: "Scimitar Notebook",
					extensions: ['scn']
				}
			]
		});
	}

	if(!file){
		win.webContents.send('saveFileDialogResponse', {
			error: true,
			message: "No file",
		});
		
	} else{
		fs.writeFileSync(file, content);

		win.webContents.send('saveFileDialogResponse', {
			error: false,
			message: file
		});
	}
})

ipcMain.on('openFileDialog', (event, file) => {
	if(!file){
		file = dialog.showOpenDialogSync(win, {
			properties: ['openFile'],
			filters: [
				{
					name: "Scimitar Notebook",
					extensions: ['scn']
				}
			]
		});
	}

	if(!file || !file.length){
		win.webContents.send('openFileDialogResponse', {
			error: true,
			message: "No file",
		});
		
	} else{
		let content = fs.readFileSync(file[0]);

		win.webContents.send('openFileDialogResponse', {
			error: false,
			message: {
				file: file[0],
				content: content
			}
		});
	}
})

ipcMain.on('showError', (event, title, content) => {
	dialog.showErrorBox(title, content);
})

ipcMain.on('showInfo', (event, title, content) => {
	dialog.showMessageBoxSync(win, {
		title: title,
		message: content
	});
})